package com.springboot.springboot_mybatis.dao;

import org.apache.ibatis.annotations.Param;

import com.springboot.springboot_mybatis.bean.City;

/**
 * 城市接口类
 * @author Administrator
 *
 */
public interface CityDao {
	/**
	 * 根据城市名称查询城市信息
	 */
	City findByName(@Param("cityname") String cityname);
}
