package com.springboot.springboot_mybatis.service;

import com.springboot.springboot_mybatis.bean.City;

/**
 * 城市业务逻辑接口类
 * @author Administrator
 *
 */
public interface CityService {
	/**
	 * 根据城市名称查询城市信息
	 * @parmm cityname
	 */
	City findByName(String cityname);
}
