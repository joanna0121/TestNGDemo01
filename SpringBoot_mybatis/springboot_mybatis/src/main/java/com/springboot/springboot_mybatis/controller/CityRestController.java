package com.springboot.springboot_mybatis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.springboot_mybatis.bean.City;
import com.springboot.springboot_mybatis.service.CityService;


@RestController
@RequestMapping("/Mybatis")
public class CityRestController {
	@Autowired
	private CityService cityService;
	
	@RequestMapping("/api/city/findOneCity")
	public City findOneCity(@RequestParam(value = "cityname", required = true) String cityname) {
		return cityService.findByName(cityname);
	}
}
