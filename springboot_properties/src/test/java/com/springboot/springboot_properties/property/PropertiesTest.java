package com.springboot.springboot_properties.property;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class PropertiesTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesTest.class);
	
	@Autowired
	private UserProperties userProperties;
	@Autowired
	private HomeProperties homeProperties;
	
	@Test
    public void getHomeProperties() {
        LOGGER.info("\n\n" + homeProperties.toString() + "\n");
    }

    @Test
    public void randomTestUser() {
        LOGGER.info("\n\n" + userProperties.toString() + "\n");
    }
	 
}
