package com.springboot.springboot_properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.springboot.springboot_properties.property.HomeProperties;

@SpringBootApplication
public class Application implements CommandLineRunner{
	@Autowired
	private HomeProperties homeProperties;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	public void run(String... arg0) throws Exception {
		System.out.println("\n" + homeProperties.toString());
		System.out.println();
	}

}
