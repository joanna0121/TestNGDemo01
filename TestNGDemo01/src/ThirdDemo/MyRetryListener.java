package ThirdDemo;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.annotations.ITestAnnotation;

public class MyRetryListener implements IAnnotationTransformer{
//	@Override
//	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
//		//通过getRetryAnalyzer()获取重跑的次数的属性
//		IRetryAnalyzer myRetry = annotation.getRetryAnalyzer();
//		//如果重跑次数为空 就自定义
//		if(myRetry == null) {
//			annotation.setRetryAnalyzer(MyRetry.class);
//		}
//	}
	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
		if(testMethod.getName().equals("test03")) {
			annotation.setInvocationCount(3);//test03这个方法调用3次
		}
	}

}
