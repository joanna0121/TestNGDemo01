package ThirdDemo;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class MyRetry implements IRetryAnalyzer{
	private int retryCount = 1;//设置当前失败执行的次数
	private static int maxRetryCount = 3;//设置最大失败执行次数
	
	@Override
	public boolean retry(ITestResult result) {
		if(retryCount < maxRetryCount) {
			System.out.println("重跑前失败执行次数：" + retryCount);
			retryCount ++ ;//再执行 则当前失败执行次数增加
			System.out.println("重跑后失败执行次数：" + retryCount);
			return true;//true则执行失败重跑
		}
		return false;//false则不重跑
	}

}
