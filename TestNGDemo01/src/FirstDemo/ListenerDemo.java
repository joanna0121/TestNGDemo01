package FirstDemo;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.Test;

public class ListenerDemo implements ITestListener{
	@Override
	public void onTestStart(ITestResult result) {
		System.out.println("用例启动。");
		System.out.println(result.toString());
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("用例执行成功。");
		System.out.println(result.toString());
	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("用例执行失败，启动截图。");
		System.out.println(result.toString());
		//调用截图方法
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}
	
	@Test
	public void listenerExampleTest() {
		System.out.println("执行测试");
		Assert.assertTrue(1==2);//报错
	}

}
