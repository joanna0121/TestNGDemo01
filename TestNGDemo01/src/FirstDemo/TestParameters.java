package FirstDemo;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestParameters {
	@Parameters({"Browser", "Server"})
	@Test
	public void test1(String browser, String server) {
		System.out.println("hello");
		System.out.println("这次启动浏览器是：" + browser + "测试服务器是：" + server);
	}
}
