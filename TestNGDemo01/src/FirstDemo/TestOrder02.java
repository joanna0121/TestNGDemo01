package FirstDemo;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestOrder02 {
	private int a;
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		a = 2;//����ֵ
		System.out.println("this is beforeMethod. the value of a is:" + a);
	}
	
	@BeforeClass
	public void beforeClass() {
		a = 1;//����ֵ
		System.out.println("this is beforeClass. the value of a is:" + a);
	}
	
	@Test(groups="TestOrder02")
	public void testMethod01() {
		a=3;//����ֵ
		System.out.println("this is test method01. the value of a is:" + a);
	}
	
	@Test(groups="TestOrder02")
	public void testMethod02() {
		a=4;//����ֵ
		System.out.println("this is test method02. the value of a is:" + a);
	}
	
	@AfterClass
	public void afterClass() {
		a = 5;//����ֵ
		System.out.println("this is afterClass. the value of a is:" + a);
	}
	
	@AfterMethod
	public void afterMethod() {
		a = 6;//����ֵ
		System.out.println("this is afterMethod. the value of a is:" + a);
	}
	
}
