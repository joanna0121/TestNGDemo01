package FirstDemo;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestDependsOnGroups01 {
	//硬依赖
//	@Test(dependsOnMethods = "tomcatServerIsDown")
//	public void restartTomcatService() {
//		System.out.println("Restart the tomcat server when it is down");
//	}
//	@Test
//	public void tomcatServerIsDown() {
//		System.out.println("tomcat service is down");
//	}
	
	//通过在@Test注释中添加“alwaysRun=true”来获得软依赖
	@Test(groups = {"tomcat"})
	public void restartTomcatService() {
		System.out.println("Restart the tomcat server when it is down");
	}
	@Test(groups = {"tomcat"})
	public void tomcatServerIsDown() {
		System.out.println("tomcat service is down");
		Assert.assertEquals(10, 11, "not equals");//会报错
	}
	@Test(dependsOnGroups = {"tomcat"}, alwaysRun = true)
	public void startAppServer() {
		System.out.println("Start App Server");
	}
}
