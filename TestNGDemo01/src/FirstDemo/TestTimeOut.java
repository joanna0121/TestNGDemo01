package FirstDemo;

import org.testng.annotations.Test;

public class TestTimeOut {
	@Test(timeOut = 3000)
	public void loginTest() {
		try {
			Thread.sleep(2800);
		} catch(InterruptedException e) {
			System.out.println(e.toString());
		}
	}
}
