package FirstDemo;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestOrder01 {
	@BeforeClass
	public void setup() {
		System.out.println("启动测试的前提条件准备，一般放这个方法中");
	}
	
	@Test
	public void test() {
		System.out.println("test order 01");
	}

	@AfterClass
	public void teardown() {
		System.out.println("测试运行结束后的步骤，一般是恢复环境到测试开始之前的状态");
	}
}
