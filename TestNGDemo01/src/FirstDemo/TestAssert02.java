package FirstDemo;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class TestAssert02 {
	@Test
	public void testSoftAssert01() {
		System.out.println("Test start");
		SoftAssert assertion = new SoftAssert();
		assertion.assertEquals(12, 27, "二者不等");
		System.out.println("Test complete");//还输出 说明有继续执行
		System.out.println(3+8);//还输出 说明有继续执行
		assertion.assertAll();// 一定要调用 否则不会去断言
	}
	
//	@Test
//	public void testSoftAssert02() {
//		String str1 = {""};
//	}
}
