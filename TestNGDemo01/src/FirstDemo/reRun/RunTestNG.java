package FirstDemo.reRun;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;

public class RunTestNG {
	public static void main(String[] args) throws InterruptedException {
		TestNG testNG = new TestNG();
		List<String> suites = new ArrayList<String>();
		suites.add(".\\src\\resources\\testng-reRun.xml");
		testNG.setTestSuites(suites);
		testNG.run();
		
		//等待执行结束，然后执行失败用例
		TestNG testNG1 = new TestNG();
		List<String> suites1 = new ArrayList<String>() ;
		Thread.sleep(5000);
		suites1.add(".\\test-output\\testng-failed.xml");
		testNG1.setTestSuites(suites1);
		testNG1.run();
	}
}
