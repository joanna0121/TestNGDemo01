package FirstDemo;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestBeforeAndAfterGroups {
	@BeforeClass
	public void init() {
		System.out.println("启动测试的前提条件准备，一般放这个方法中");
	}
	
	@AfterClass
	public void finish() {
		System.out.println("测试运行结束后的步骤，一般是恢复环境到测试开始之前的状态");
	}
	
	@BeforeGroups(groups = {"groupB"})
	public void setup() {
		System.out.println("Method---setup");
	}
	
	@AfterGroups(groups = {"groupB"})
	public void teardown() {
		System.out.println("Method---teardown");
	}
	
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("before method");
	}
	
	@AfterMethod
	public void afterMethod() {
		System.out.println("after method");
	}
	
	@Test(groups = {"groupA"})
	public void testMethod1() {
		System.out.println("testMethod1");
	}
	
	@Test(groups = {"groupB"})
	public void testMethod2() {
		System.out.println("testMethod2");
	}
	
	@Test(groups = {"groupB"})
	public void testMethod3() {
		System.out.println("testMethod3");
	}
}
