package FirstDemo;

import org.testng.annotations.Test;

public class TestGroups {
	@Test(groups = {"API Test", "Function Test"})
	public void test01() {
		System.out.println("api testing and function testing");
	}
	@Test(groups = {"API Test"})
	public void test02() {
		System.out.println("api testing");
	}
	@Test(groups = {"Function Test"})
	public void test03() {
		System.out.println("function testing");
	}
	@Test()
	public void test04() {
		System.out.println("not in api and function testing");
	}
}
