package FirstDemo;

import org.testng.annotations.Test;

public class TestDependsOnGroups02 {
	@Test(groups = {"tomcat"}, dependsOnMethods = "tomcatServerIsDown")
	public void restartTomcatService() {
		System.out.println("Restart the tomcat server when it is down");
	}
	@Test(groups = {"tomcat"})
	public void tomcatServerIsDown() {
		System.out.println("tomcat service is down");
	}
	@Test(groups = {"app"})
	public void startAppServer() {
		System.out.println("Start App Server");
	}
	@Test(groups = {"app"}, dependsOnMethods = "startAppServer")
	public void shutDownApp() {
		System.out.println("Shutdown App Server");
	}
}
