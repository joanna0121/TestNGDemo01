package FirstDemo;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestAssert01 {
	@Test
	public void test(){
		//Assert.assertEquals(134, 134);
		//Assert.assertEquals("joanna", "Joanna", "not right");//判断两个字符串是否相等
		String[] str1 = {"my", "name", "is", "joanna"};
		String[] str2 = {"joanna", "my", "name", "is"};
		//Assert.assertEqualsNoOrder(str1, str2, "is different");//判断两个对象是否相同，忽略排序位置
		//返回为假则通过，否则输出信息
		//Assert.assertFalse(str1!=str2, "str1 is not equal with str2");//考虑排序二者不等
		//不相等则通过
		//Assert.assertNotEquals(str1, str2, "str1 is equal with str2");
//		String nullStr = null;
//		Assert.assertNotNull(nullStr, "is null");
		// equals是值比较，same是内存地址比较
		String[] str3 = str1;//指向同一个内存位置
		String[] str4 = new String[]{"my", "name", "is", "joanna"};
//		System.out.println(str1==str3);//true
//		System.out.println(str1==str2);//false
//		System.out.println(str1==str4);//false
		Assert.assertSame(str1, str3, "str1 not the same as str3");
//		Assert.assertSame(str1, str2, "str1 not the same as str2");
		Assert.assertSame(str1, str4, "str1 not the same as str4");
		//Assert.assertNotSame(str1, str2, "same");
//		Assert.assertTrue(str1==str2, "not same");
		 
	}
}
