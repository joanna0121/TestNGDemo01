package FirstDemo;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

public class TestFactoryDataProvider {
	private int n;
	@Factory(dataProvider = "dp")
	public TestFactoryDataProvider(int n) {
		this.setN(n);
	}
	
	@DataProvider
	public static Object[][] dp() {
		Object[][] result = new Object[1][2];
//		return new Object[][] {
//			new Object[] {41},
//			new Object[] {42}
//		};
		
		return result;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}
}
