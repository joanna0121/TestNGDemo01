package FirstDemo;

import org.testng.annotations.Test;

public class TestInvocation {
	@Test(invocationCount = 5, invocationTimeOut = 4900)
	public void loginTest () {
		try {
			Thread.sleep(1000);
			System.out.println("login test");
		} catch(InterruptedException e) {
			System.out.println(e.toString());
		}
	}
}
