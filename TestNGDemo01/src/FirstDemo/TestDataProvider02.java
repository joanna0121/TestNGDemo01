package FirstDemo;

import org.testng.annotations.DataProvider;

/*
 * 测试将data provider放在不同类
 */
public class TestDataProvider02 {
	@DataProvider(name = "create")
	public static Object[][] createData() {
		return new Object[][] {
			new Object[] {
					new Integer(21)
			}
		};
	}
}
